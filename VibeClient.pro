QT += core
QT -= gui

TARGET = test1
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    vibe.cpp \
    utils.cpp

INCLUDEPATH += /usr/local/include/opencv
INCLUDEPATH += /usr/local/include/opencv2


LIBS += -L/usr/local/lib \
    -lopencv_calib3d \
    -lopencv_core \
    -lopencv_features2d \
    -lopencv_flann \
    -lopencv_highgui \
    -lopencv_imgproc \
    -lopencv_ml \
    -lopencv_objdetect \
    -lopencv_video \
    -lopencv_videoio \
    -lopencv_imgcodecs

QMAKE_CXXFLAGS += -std=c++11

HEADERS += \
    vibe.h \
    utils.h
